package com.zuitt;

import java.util.Date;

public class Main {
    public Main(){

    }
    public static void main(String[] args) {
        User user1 = new User("Terence Gaffud", 25, "tgaff@mail.com", "Quezon City");

        Course course1 = new Course();
        course1.setName("MACQ004");
        course1.setDescription("An introduction to Java for career-shifters.");
        course1.setFee(10000);
        course1.setSeats(100);
        course1.setInstructor(user1);
        course1.setStartDate(new Date(2023,10,07));
        course1.setEndDate(new Date(2023,14,07));

        System.out.println("Hi! I'm " + user1.getName() + ". I'm " + user1.getAge() + " years old. You can reach me via my email: " + user1.getEmail()
                + ". When I'm off work, I can be found at my hose in " + user1.getAddress() + "." );

        System.out.println("Wecome to the course " + course1.getName() + ". This course can be described as " + course1.getDescription() +
                " Your instructor for this course is Sir " + course1.getInstructor().getName() + ". Enjoy!" );

    }
}
