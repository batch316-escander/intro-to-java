package com.zuitt;
import java.util.Scanner;

public class activity {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");

        double inputYear = new Double(myObj.nextLine());
        double remainder = inputYear % 4;
        if(remainder==0) {
            System.out.println(Math.round(inputYear) + " is a leap year");
        } else {
            System.out.println(Math.round(inputYear) + " is NOT a leap year");
        }
    }
}
