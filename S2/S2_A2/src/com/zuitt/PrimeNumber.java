package com.zuitt;

import java.util.ArrayList;
import java.util.HashMap;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeArray = new int[5];
        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;

        System.out.println("The first prime number is: " + primeArray[0]);

        ArrayList<String> names = new ArrayList();
        names.add("John");
        names.add("Jane");
        names.add("Chloe");
        names.add("Zoey");

        System.out.println("My friends are: (" + names.get(0) + ", " + names.get(1) + ", " + names.get(2) + ", " + names.get(3) +")");

        HashMap<String, Integer> items = new HashMap();
        items.put("toothpaste", 15);
        items.put("toothbrush", 20);
        items.put("soap", 12);
        System.out.println("Our current inventory consists of: " + items);

    }
}
