import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // WHILE LOOP
        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");
        try {
            int num = in.nextInt();
            int answer = 1;
            int counter = 1;

            while (counter <= num){
                if(num > 0){
                    answer *= counter;
                }
                counter+=1;
            };

            if(num >0) {
                System.out.println(answer);
            } else {
                System.out.println("Integer should be greater than Zero");
            }
        } catch (Exception e) {
            System.out.println("Invalid Input");
            e.printStackTrace();
        }

        // FOR LOOP
        Scanner in2 = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");

        try {
            int num2 = in2.nextInt();
            int answer2 = 1;


            for (int i = 1; i <= num2; i++) {
                if(num2 > 0){
                    answer2 *= i;
                }
            }
            if(num2 >0) {
                System.out.println(answer2);
            } else {
                System.out.println("Integer should be greater than Zero");
            }
        } catch (Exception e) {
            System.out.println("Invalid Input");
            e.printStackTrace();
        }

        //
        for (int i = 1; i<=5;i++){
            for(int j=1; j<=i;j++){
                if(j != i){
                    System.out.print("* ");
                } else {
                    System.out.println("* ");
                }
            }
        }


    }
}