package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in); // Create a Scanner object
        System.out.print("Enter a username: ");

        String username = myObj.nextLine();
        System.out.println("Username is: " + username);
    }
}
