package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        double averageGrade;

        Scanner myObj = new Scanner(System.in);

        System.out.println("First name: ");
        firstName = myObj.nextLine();

        System.out.println("Last name: ");
        lastName = myObj.nextLine();

        System.out.println("First Subject Grade: ");
        firstSubject = new Double(myObj.nextLine());

        System.out.println("Second Subject Grade: ");
        secondSubject = new Double(myObj.nextLine());

        System.out.println("Third Subject Grade: ");
        thirdSubject = new Double(myObj.nextLine());

        averageGrade = (firstSubject + secondSubject + thirdSubject)/3;

        System.out.println("Good day " + firstName + " " + lastName + "." );
        System.out.println("Your grade average is: " + averageGrade);
    }
}
